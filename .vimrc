"
"
" ~/.vimrc
" Daniel Müller www.github.com/daniel-mueller
" last updated: September, 4th, 2013.
"

set nocompatible
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" filetype plugins
filetype plugin on
filetype indent on

" auto/smart indenting
set ai
set si

"Remembers 800 lines of history
set history=800

"show current position and line-numbers
set ruler
set cursorline
set nu

"show matching braces
set showmatch

"show commands
set showcmd

"Syntax highlighting
syntax on

"some indentation rules
set sw=4
set ts=4
set sts=4

"Ignore case when searching
set ignorecase

"highlight search results
set hlsearch

"Colors and Fonts:
colorscheme desert
set background=dark

"extraoptions for Gvim
if has("gui_running")
	set guioptions-=T
	set guioptions+=e
	set t_Co=256
	set guitablabel=%M\ %t
endif

"encoding
set encoding=utf8

"no backup:
set nobackup
set nowb
set noswapfile


